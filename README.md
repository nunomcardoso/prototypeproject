# Table of Contents
* [Organization](#Organization)
* [Generate SQL and POCO](#Generate-SQL-and-POCO)
* [Setup database](#Setup-database)
* [❗❗ Before everything ❗❗](#❗❗-Before-everything-❗❗)
    * [Automapper](#Automapper)
    * [RepoDb](#RepoDb)
* [Adding new tables to database (upgrade)](#Adding-new-tables-to-database-(upgrade))
* [Creating a repository](#Creating-a-repository)
* [Creating a service](#Creating-a-service)
* [Creating a MVC Controller](#Creating-a-MVC-Controller)
* [Cache](#Cache)
* [Logging](#Logging)
* [FAQ](#FAQ)

# Organization
| Project | Main focus | Dependencies |
| -- | -- | -- |
| DAL.Model | Replication in POCO of the domain entities in the database. | |
| DAL.ExternalModel | Models that the application receive as input (**_input models_**) and models that the application exposes (**_view models_**). |  |
| DAL.Access | Connects to the database and performs CRUD operations.<br>Exposes _Entity_ repositories with CRUD operations. |<li>RepoDb<li>DAL.Model |
| DAL.Service | Makes validations in order to access the database. Performs CRUD operations transactionally. <br>Also encapsulates operations with **_domain entities_**, only exposing **_view models_** and receving **_input models_**.<br>Also maps `domain -> viewmodel` and `input -> model`. | <li>AutoMapper<li>DAL.Model<li>DAL.Access<li>DAL.ExternalModel |
| MVC | Exposes controllers via HTTP, these controllers use the services in `DAL.Service`. | <li>DAL.ExternalModel<li>DAL.Service |


# Generate SQL and POCO
Open `Visual Studio` with the `vuerd` extension, or use the [online version](https://vuerd.github.io/vuerd/).
Import the `PrototypeDB.vuerd.json` and select **SQL DLL** and **Generate Code** for C#.
C# entities POCO go to `DAL.Model.Entities.cs`.


# Setup database
Execute the DDL created by `vuerd` into the database (e.g. `PrototypeDB`).<br>
Grab the connection string for that database (e.g.. `Server=.;Database=PrototypeDB;Integrated Security=SSPI;`), this connection string is used in `Prototype.DAL.Service.BaseService.cs`.


# ❗❗ Before everything ❗❗
Some initial configurations must be performed in order to all work smoothly.

## Automapper 
Documentation: [reference](https://automapper.readthedocs.io/en/latest/Setup.html)

Create Mappings for the `Domain -> ViewModel` and `InputModel -> Domain`. Now is being do in `Prototype.DAL.ExternalModel.EntityMapperProfile.cs`.

## RepoDb
Documentation: [reference](https://repodb.readthedocs.io/en/latest/pages/installation.html)

Blog post w/ examples: [reference](https://dev.to/mikependon/c-what-will-make-you-choose-repodb-over-dapper-orm-3eb8)

In the initialization of your application should be like this (for `SQLServer`):
* Console Application (`Main() method`):
``` C#
    public static void Main(){
        // RepoDB
        SqlServerBootstrap.Initialize();

        // [ ... ]
    }
```
* ASP .NET MVC (`Global.asx.cs`):
``` C#
    protected void Application_Start()
    {
        // [ ... ]

        // RepoDB
        SqlServerBootstrap.Initialize();
    }
```

# Adding new tables to database (upgrade)
Simple steps to add new entities:
1. Go to [Generate SQL and POCO](#Generate-SQL-and-POCO) and create the new tables in database then add the new POCO to `DAL.Model.Entities`. 
2. Create a new Repository [Creating a repository](#Creating-a-repository).
3. Create the `ViewModels` and `InputModels`
3. Create a new Service [Creating a service](#Creating-a-service).
4. Finally call your new service to perform operations.

Altering collumns in a table:
1. Go to [Generate SQL and POCO](#Generate-SQL-and-POCO) and alter the table in database then add the _new_ POCO to `DAL.Model.Entities`. 
2. If needed add the new attribute to `ViewModel` and `InputModel`.
3. Verify if it needs refactoring in the `Repository` and `Service` to affect the new attribute.


# Creating a repository
1. Create an `interface` with the operations to perform:
 ``` C#
public interface IBankAccountRepository
{
    BankAccout GetAccount(int accountId);
    IEnumerable<BankAccout> GetAllAccounts();
    int CreateAccount(BankAccout account, IDbTransaction transaction);
    int DeleteStudent(int accountId, IDbTransaction transaction);
}
```
2. Inherit from `GenericRepository` and implement the previously created `interface`:
``` C#
public class BankAccountRepository : GenericRepository<BankAccount>, IBankAccountRepository
{
    public BankAccountRepository(string connectionString) : base(connectionString) { }

    // SELECT * FROM BankAccount WHERE accountId = @accountId
    public BankAccout GetAccount(int accountId)
    {
        return Query(accountId).FirstOrDefault();
    }

    // SELECT * FROM BankAccount
    public IEnumerable<BankAccout> GetAllAccounts()
    {
        return QueryAll();
    }

    // INSERT INTO BankAccount (accountId, money) VALUES(0, '1000');
    public int CreateAccount(BankAccout account, IDbTransaction transaction)
    {
        return Insert<int>(account, transaction: transaction);
    }

    // DELETE FROM BankAccount WHERE accountId=0;
    public int DeleteAccount(int accountId, IDbTransaction transaction)
    {
        return Delete(accountId, transaction: transaction);
    }
}
```

3. **(Optional)** For entities with few operations can be a little overkill to create a new `interface` and inherit the `GenericRepository`. To simplify this process just use `GenericRepository<T>` being `T` the entity, it exposes the same operation as above. This should only be used in other repositories that need to perform operations in another Entity. Just like this:
``` C#
public class BankAccountRepository
{
    // [ ... ] previous CRUD operations

    protected GenericRepository<BankAccountPerson> BankAccountPersonRepository { get; set; }

    public int AddPersonToAccount(int accountId, int personId, IDbTransaction transaction)
    {
        return BankAccountPersonRepository.Insert<int>(
            new BankAccountPerson
            {
                BankAccountId = accountId,
                PersonId = personId
            }, 
            transaction: transaction
        );
    }
}
```

4. **(Optional)** RAW SQL operations:
 ``` C#
public class BankAccountRepository
{
    // [ ... ] previous CRUD operations
    
    public BankAccount GetBankAccoutRaw(int accountId)
    {
        var connection = CreateConnection();

        var commandText = @"SELECT accountId, money FROM [PrototypeDB].[dbo].[Class] WHERE accountId = @accountId;";
        var param = new {
            accountId = accountId,
            // more parameters if needed
        };

        return connection.ExecuteQuery<BankAccount>(commandText, param).FirstOrDefault();
    }
}
```


# Creating a service
1. Create operations that your presentation layer will execute.
2. Instantiate, in the constructor, Lazy repositories that will be used to perform the operations. When accessing the repositories use the properties, **not** the `Lazy` objects:
```C#
public class MockService : BaseService 
{
#region LAZY
    private Lazy<IOneRepository> _oneRepo;
    private Lazy<ITwoRepository> _twoRepo;
    private Lazy<GenericRepository<Three>> _threeRepo;
#endregion LAZY

    protected IOneRepository OneRepository => _oneRepo.Value;
    protected ITwoRepository TwoRepository => _twoRepo.Value;
    protected <GenericRepository<Three> ThreeRepository => _threeRepo.Value;

    // protected string connectrionStr; // already defined in base class
    public BaseService() : base()
    {
        // Instantiate lazy repositories
        _oneRepo     = new Lazy<IOneRepository> ( ()=> new OneRepository(connectionStr) );
        _twoRepo     = new Lazy<ITwoRepository> ( ()=> new TwoRepository(connectionStr) );
        _threeRepo   = new Lazy<GenericRepository<Three>> ( ()=> new GenericRepository<Three>(connectionStr) );
    }
}
```
3. The method parameters should be `InputModels` and/or `Value Types`, and the return type should be `ViewModel`:
```C#
    public IEnumerable<StudentVM> GetStudents()
    {
        var model = StudentRepository.GetAllStudents();
        var vm = Map<IEnumerable<StudentVM>>(model);

        return  vm;
    }

    // Point 3. shows how to use transactions for write operations.
    public StudentVM CreateStudent(StudentIM StudentInput)
    {
        var model = Map<Student>(StudentInput);
        var result = StudentRepository.CreateStudent(model);
        var vm = Map<StudentVM>(result);

        return vm;
    }
```
4. Write operations should always be performed in the scope of a transaction, facilitating rollback if something goes wrong, like this:
```C#
        // Uow disposes of the connection and transaction
        using (Uow) {
            try {
                var transaction = Uow.BeginAndGetTransaction();
                
                // YOUR WRITE OPERATION.(model, transaction);
                
                Uow.Save();

                return ifNeeded;
            }
            catch (SqlException e){
                Uow.Rollback();
                throw ExceptionHandlerAndThrower.ExceptionFromSQLServerError(e);;
            }
        }
```
4. **(Optional)** When your `Service` connects to another database, use another connection string. The `BaseService` constructor receives an _optional_ connection string.
```C#
    public BaseService(string connStr = connectionString);
```
```C#
public class ClassService : BaseService
{
    private const string external_connStr = @"Server=.;Database=EXTERNAL_DB;Integrated Security=SSPI;";

    public ClassService() : base(external_connStr)
    { }
}
```


# Creating a MVC Controller
1. Create the controller, in this example will be an API controller.
2. Your controllers should do as **little** as possible, since they are the presentation layer:
```C#
public class ClassController : ApiController
{
    ClassService service = new ClassService();

    // GET api/class
    public IHttpActionResult Get()                          => Ok(service.GetClasses());
    // GET api/class/5
    public IHttpActionResult Get(int id)                    => Ok(service.GetClass(id));
    // POST api/class
    public IHttpActionResult Post([FromBody]ClassIM value)  => Created("todo", service.CreateClass(value));
    // DELETE api/class/5
    public IHttpActionResult Delete(int id)                 => Ok(service.DeleteClass(id));
}
```

# Cache
RepoDb has the ability to cache. [Reference](https://dev.to/mikependon/increasing-c-data-access-performance-by-90-using-repodb-2nd-layer-cache-4kip)


# Logging
[Reference](https://michaelscodingspot.com/logging-in-dotnet/)

When Logging use `#if DEGUB` for `_log.Debug` and `_log.Info`


# FAQ
### What if I want to `INSERT`/`DELETE` into/from two+ tables in the same transaction?
* Create a new `Service` and wrap the whole `INSERT`, `DELETE`, `UPDATE` or `SELECT` in the same transaction. 
* In the constructor instantiate the `Lazy` objects of the repositories you want to manipulate. 
* Copy the transactional structure from [Creating a service](#Creating-a-service) point 4.
* Just call your new operation, `Service.WriteMultipleTables()`.

[▲ Top ▲](#Table-of-Contents)